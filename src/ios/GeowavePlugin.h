 #import <Cordova/CDV.h>
#import "Geowave.h"

@interface GeowavePlugin : CDVPlugin

- (void)initWithAppID:(CDVInvokedUrlCommand *)command;

- (void)requestLocationAuthorization:(CDVInvokedUrlCommand *)command;

- (void)setPushSettings:(CDVInvokedUrlCommand *)command;

- (void)deviceIDFA:(CDVInvokedUrlCommand *)command;

- (void)createSegmentsWithName:(CDVInvokedUrlCommand *)command;

- (void)removeSegmentsWithName:(CDVInvokedUrlCommand *)command;

- (void)removeAllSegments:(CDVInvokedUrlCommand *)command;

- (void)createFunnelWithName:(CDVInvokedUrlCommand *)command;

- (void)removeAllFunnels:(CDVInvokedUrlCommand *)command;

@end