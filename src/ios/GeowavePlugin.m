#import "GeowavePlugin.h"

@implementation GeowavePlugin

- (void)initWithAppID:(CDVInvokedUrlCommand *)command {
  NSMutableDictionary *options = [command.arguments objectAtIndex:0];
  
  int appID = [[options objectForKey:@"appID"] intValue];
  
  [Geowave initWithAppID:appID];
  
  CDVPluginResult *result = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK];
  
  [self.commandDelegate sendPluginResult:result callbackId:command.callbackId];
}

- (void)requestLocationAuthorization:(CDVInvokedUrlCommand *)command {
  [Geowave requestLocationAuthorization];
  
  CDVPluginResult *result = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK];
  
  [self.commandDelegate sendPluginResult:result callbackId:command.callbackId];
}

- (void)setPushSettings:(CDVInvokedUrlCommand *)command {
  NSMutableDictionary *options = [command.arguments objectAtIndex:0];
  
  NSString *pushToken = [options objectForKey:@"pushToken"];
  BOOL *hasPushEnabled = YES;
  
  if([pushToken length] == 0)
    hasPushEnabled = NO;
  
  [Geowave setPushSettings:pushToken:hasPushEnabled];
  
  CDVPluginResult *result = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK];
  
  [self.commandDelegate sendPluginResult:result callbackId:command.callbackId];
}

- (void)deviceIDFA:(CDVInvokedUrlCommand*)command {
  NSString* myString = Geowave.deviceIDFA;
  
  CDVPluginResult* result = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:myString];
  [self.commandDelegate sendPluginResult:result callbackId:command.callbackId];
}

- (void)createSegmentsWithName:(CDVInvokedUrlCommand *)command {
  NSMutableDictionary *options = [command.arguments objectAtIndex:0];
  
  NSString *name = [options objectForKey:@"name"];
  NSArray *values = [options objectForKey:@"values"];
  
  [Geowave createSegmentsWithName:name andValues:values];
  
  CDVPluginResult *result = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK];
  
  [self.commandDelegate sendPluginResult:result callbackId:command.callbackId];

}

- (void)removeSegmentsWithName:(CDVInvokedUrlCommand *)command {
  NSMutableDictionary *options = [command.arguments objectAtIndex:0];
  
  NSString *name = [options objectForKey:@"name"];
  NSArray *values = [options objectForKey:@"values"];
  
  [Geowave removeSegmentsWithName:name andValues:values];
  
  CDVPluginResult *result = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK];
  
  [self.commandDelegate sendPluginResult:result callbackId:command.callbackId];
}

- (void)removeAllSegments:(CDVInvokedUrlCommand *)command {
  
  [Geowave removeAllSegments];
  
  CDVPluginResult *result = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK];
  
  [self.commandDelegate sendPluginResult:result callbackId:command.callbackId];
}

- (void)createFunnelWithName:(CDVInvokedUrlCommand *)command {
  
  NSMutableDictionary *options = [command.arguments objectAtIndex:0];
  
  NSString *name = [options objectForKey:@"name"];
  NSInteger *level = [[options objectForKey:@"level"] integerValue];
  
  [Geowave createFunnelWithName:name andLevel:level];
  
  CDVPluginResult *result = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK];
  
  [self.commandDelegate sendPluginResult:result callbackId:command.callbackId];
  
}

- (void)removeAllFunnels:(CDVInvokedUrlCommand *)command {
  
  [Geowave removeAllFunnels];
  
  CDVPluginResult *result = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK];
  
  [self.commandDelegate sendPluginResult:result callbackId:command.callbackId];
}

@end